# Take-two-assignement

## Overview

This docker image can take in a access_log file to perform statistics aggregation on its contents. 
Mounting the location of the log file path is required, as docker containers will have have no knowledge of the host's filesystem. 

## Build

To build, run 
```
docker build -t taketwo .
```

This makes the app available as a docker image known as `taketwo`

## Run

To run, navigate to the folder where your access_files are located, and execute the command

```
docker run --rm -it --name taketwo -p 5000:5000 -v $(pwd):/app/stage  -t taketwo  <access_log_name>
```

You can also substitute `$(pwd)` with the directory of the log files instead of navigating to the folder. 
**Make sure to put the corresponding access log filename at the end (in place of by <access_log_name>)**

The API will be mapped to the host's port `5000`. Only `/stats` path will be available. 

Open `http://localhost:5000` on the browser in your host to see the stats for the logfile.
