#!/usr/bin/env python3
from flask import Flask, jsonify
import sys
import re

# Field parser regex for access_log files
FIELD_PARSER_REGEX = '([(\d\.)]+) - - \[(.*?)\] "(.*?)" (\d+) (\d+) "(.*?)" "(.*?)"'

if __name__ == '__main__':
    if len(sys.argv) < 2:
        # Prevent any run from occuring if no argument provided
        print("You need to provide a file to display for /stats page.")
        exit(1)
    else:
        app = Flask(__name__)
        print("Hello")

        # /stats definition
        @app.route("/stats")
        def stat():
            ip_bucket = {}
            resp_code_bucket = {}
            referrer_bucket = {}

            with open(f"stage/{sys.argv[1]}") as f:
                content = f.readlines()
            lines = [x.strip() for x in content]
            for line in lines:
                # Using the regex rule, log lines can be converted to the tuple in such format
                # (IP, access_date, req_path, return_code, bytes, referrer, useragent)
                access_tuple = re.match(FIELD_PARSER_REGEX, line).groups()
                ip = access_tuple[0]
                return_code = access_tuple[3]
                req_path = access_tuple[2]
                referrer = access_tuple[5]

                # stat aggregation logic on each line in file
                ip_bucket[ip] = ip_bucket[ip] + 1 if ip in ip_bucket else 1
                resp_code_bucket[return_code] = resp_code_bucket[return_code] + 1 if return_code in resp_code_bucket else 1
                http_method = req_path[0:3]
                if http_method == "GET":
                    referrer_bucket[referrer] = referrer_bucket[referrer]+1 if referrer in referrer_bucket else 1
            
            # grab referrers and sort them by occurance (reverse), then get only first 5
            top5_referers = sorted(referrer_bucket,key=referrer_bucket.get,reverse=True)[:5]

            # dict to convert to json
            stat_obj = {
                'unique_ips' : len(ip_bucket.keys()),
                'ip_distrib' : ip_bucket,
                'http_resp_code_distrib' : resp_code_bucket,
                'top_5_referrals_GET': top5_referers
            }
            return jsonify(stat_obj)

        # allow any host to connect            
        app.run(debug=False, host='0.0.0.0')